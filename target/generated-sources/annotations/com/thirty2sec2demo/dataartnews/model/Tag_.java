package com.thirty2sec2demo.dataartnews.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Tag.class)
public abstract class Tag_ extends com.thirty2sec2demo.dataartnews.model.BaseObject_ {

	public static volatile ListAttribute<Tag, Article> articles;
	public static volatile SingularAttribute<Tag, String> tag;

}

