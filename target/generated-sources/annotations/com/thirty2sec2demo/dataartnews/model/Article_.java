package com.thirty2sec2demo.dataartnews.model;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Article.class)
public abstract class Article_ extends com.thirty2sec2demo.dataartnews.model.BaseObject_ {

	public static volatile ListAttribute<Article, Tag> tags;
	public static volatile SingularAttribute<Article, String> content;
	public static volatile SingularAttribute<Article, String> title;
	public static volatile SingularAttribute<Article, ArticleStatus> articleStatus;
	public static volatile SingularAttribute<Article, Integer> ordering;
	public static volatile SingularAttribute<Article, Date> lastUpdatedDate;
	public static volatile SingularAttribute<Article, Date> createdDate;
	public static volatile SingularAttribute<Article, User> creator;

}

