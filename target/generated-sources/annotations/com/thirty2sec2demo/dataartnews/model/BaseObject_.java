package com.thirty2sec2demo.dataartnews.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(BaseObject.class)
public abstract class BaseObject_ {

	public static volatile SingularAttribute<BaseObject, Long> id;

}

