package com.thirty2sec2demo.dataartnews.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(ArticleStatus.class)
public abstract class ArticleStatus_ extends com.thirty2sec2demo.dataartnews.model.BaseObject_ {

	public static volatile SingularAttribute<ArticleStatus, String> status;

}

