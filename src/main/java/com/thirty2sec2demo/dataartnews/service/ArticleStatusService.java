package com.thirty2sec2demo.dataartnews.service;

import com.thirty2sec2demo.dataartnews.model.ArticleStatus;

import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
public interface ArticleStatusService {

    void create(ArticleStatus object);

    void update(ArticleStatus object);

    ArticleStatus find(long id);

    List<ArticleStatus> getAll();

    void delete(ArticleStatus object);
}
