package com.thirty2sec2demo.dataartnews.service;

import com.thirty2sec2demo.dataartnews.model.Article;

import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
public interface ArticleService {

    void create(Article object);

    void update(Article object);

    Article find(long id);

    List<Article> getAll();

    List<Article> getAllDescending();

    void delete(Article object);
    
}
