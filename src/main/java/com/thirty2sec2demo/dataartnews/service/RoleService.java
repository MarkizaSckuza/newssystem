package com.thirty2sec2demo.dataartnews.service;

import com.thirty2sec2demo.dataartnews.model.Role;

import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
public interface RoleService {

    void create(Role object);

    void update(Role object);

    Role find(long id);

    List<Role> getAll();

    void delete(Role object);
}
