package com.thirty2sec2demo.dataartnews.service;

import com.thirty2sec2demo.dataartnews.model.Tag;

import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
public interface TagService {

    void create(Tag object);

    void update(Tag object);

    Tag find(long id);

    List<Tag> getAll();

    void delete(Tag object);
}
