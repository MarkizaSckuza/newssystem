package com.thirty2sec2demo.dataartnews.service;

import com.thirty2sec2demo.dataartnews.model.User;

import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
public interface UserService {

    void create(User object);

    void update(User object);

    User find(long id);

    List<User> getAll();

    void delete(User object);

}
