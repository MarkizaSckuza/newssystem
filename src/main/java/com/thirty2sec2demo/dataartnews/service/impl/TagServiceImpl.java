package com.thirty2sec2demo.dataartnews.service.impl;

import com.thirty2sec2demo.dataartnews.model.Tag;
import com.thirty2sec2demo.dataartnews.service.TagService;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
@Stateless
@LocalBean
public class TagServiceImpl implements TagService, Serializable {
    @Override
    public void create(Tag object) {

    }

    @Override
    public void update(Tag object) {

    }

    @Override
    public Tag find(long id) {
        return null;
    }

    @Override
    public List<Tag> getAll() {
        return null;
    }

    @Override
    public void delete(Tag object) {

    }
}
