package com.thirty2sec2demo.dataartnews.service.impl;

import com.thirty2sec2demo.dataartnews.model.User;
import com.thirty2sec2demo.dataartnews.service.UserService;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Margo on 12.05.2015.
 */

@Stateless
@LocalBean
public class UserServiceImpl implements UserService, Serializable{
    @Override
    public void create(User object) {

    }

    @Override
    public void update(User object) {

    }

    @Override
    public User find(long id) {
        return null;
    }

    @Override
    public List<User> getAll() {
        return null;
    }

    @Override
    public void delete(User object) {

    }
}
