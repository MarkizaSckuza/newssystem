package com.thirty2sec2demo.dataartnews.service.impl;

import com.thirty2sec2demo.dataartnews.model.Role;
import com.thirty2sec2demo.dataartnews.service.RoleService;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
@Stateless
@LocalBean
public class RoleServiceImpl implements RoleService, Serializable {
    @Override
    public void create(Role object) {

    }

    @Override
    public void update(Role object) {

    }

    @Override
    public Role find(long id) {
        return null;
    }

    @Override
    public List<Role> getAll() {
        return null;
    }

    @Override
    public void delete(Role object) {

    }
}
