package com.thirty2sec2demo.dataartnews.service.impl;

import com.thirty2sec2demo.dataartnews.model.ArticleStatus;
import com.thirty2sec2demo.dataartnews.service.ArticleStatusService;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
@Stateless
@LocalBean
public class ArticleStatusServiceImpl implements ArticleStatusService, Serializable{
    @Override
    public void create(ArticleStatus object) {

    }

    @Override
    public void update(ArticleStatus object) {

    }

    @Override
    public ArticleStatus find(long id) {
        return null;
    }

    @Override
    public List<ArticleStatus> getAll() {
        return null;
    }

    @Override
    public void delete(ArticleStatus object) {

    }
}
