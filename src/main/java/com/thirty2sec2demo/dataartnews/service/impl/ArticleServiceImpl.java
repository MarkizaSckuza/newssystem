package com.thirty2sec2demo.dataartnews.service.impl;

import com.thirty2sec2demo.dataartnews.dao.impl.ArticleDAOImpl;
import com.thirty2sec2demo.dataartnews.model.Article;
import com.thirty2sec2demo.dataartnews.service.ArticleService;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */

@Stateless
@LocalBean
public class ArticleServiceImpl implements ArticleService, Serializable {

    @Inject
    private ArticleDAOImpl articleDAO;


    @Override
    public void create(Article object) {
        articleDAO.create(object);
    }

    @Override
    public void update(Article object) {
        articleDAO.update(object);
    }

    @Override
    public Article find(long id) {
        return articleDAO.find(id);
    }

    @Override
    public List<Article> getAll() {
        return articleDAO.getAll();
    }

    @Override
    public List<Article> getAllDescending() {
        return articleDAO.getAllDescending();
    }

    @Override
    public void delete(Article object) {
        articleDAO.delete(object);
    }
}
