package com.thirty2sec2demo.dataartnews.view.beans;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * Created by Margo on 12.05.2015.
 */
@Named
@ViewScoped
public class LinksBean implements Serializable {

    private static final String MAIN_PATH = "/Main.xhtml";
    private static final String ADMIN_PATH = "admin/AdminApplication.xhtml";
    private static final String MAIN = "MAIN";
    private static final String ADMIN = "ADMIN";

    private String path;
    private String linkName;

    @PostConstruct
    public void init() {
        HttpServletRequest origRequest = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
        String link = origRequest.getRequestURL().toString();
        if (link.contains("Main.xhtml")) {
            setPath(ADMIN_PATH);
            setLinkName(ADMIN);
        } else {
            setPath(MAIN_PATH);
            setLinkName(MAIN);
        }
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getLinkName() {
        return linkName;
    }

    public void setLinkName(String linkName) {
        this.linkName = linkName;
    }
}
