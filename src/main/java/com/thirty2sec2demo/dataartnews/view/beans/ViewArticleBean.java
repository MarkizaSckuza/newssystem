package com.thirty2sec2demo.dataartnews.view.beans;

import com.thirty2sec2demo.dataartnews.model.Article;
import com.thirty2sec2demo.dataartnews.service.impl.ArticleServiceImpl;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Margo on 12.05.2015.
 */
@Named
@ViewScoped
public class ViewArticleBean implements Serializable {

    @Inject
    private ArticleServiceImpl articleService;

    @Inject
    private Article article;

    private Boolean isAbsent;

    @PostConstruct
    public void init() {
        String articleId = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap().get("id");
        if (!articleId.isEmpty() && articleId != null) {
            article = articleService.find(Long.valueOf(articleId));
        } else {
            setIsAbsent(true);
        }
    }

    public ArticleServiceImpl getArticleService() {
        return articleService;
    }

    public void setArticleService(ArticleServiceImpl articleService) {
        this.articleService = articleService;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public Boolean getIsAbsent() {
        return isAbsent;
    }

    public void setIsAbsent(Boolean isAbsent) {
        this.isAbsent = isAbsent;
    }
}
