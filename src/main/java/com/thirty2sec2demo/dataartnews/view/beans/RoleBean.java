package com.thirty2sec2demo.dataartnews.view.beans;

import com.thirty2sec2demo.dataartnews.dao.impl.RoleDAOImpl;
import com.thirty2sec2demo.dataartnews.model.Role;

import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;

/**
 * Created by Margo on 10.05.2015.
 */
@Named
@ViewScoped
public class RoleBean implements Serializable {

    @Inject
    private RoleDAOImpl roleDAO;

    @Inject
    private Role role;

    public void create() {
        roleDAO.create(role);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }
}