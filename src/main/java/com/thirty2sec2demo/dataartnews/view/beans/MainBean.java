package com.thirty2sec2demo.dataartnews.view.beans;

import com.thirty2sec2demo.dataartnews.model.Article;
import com.thirty2sec2demo.dataartnews.service.impl.ArticleServiceImpl;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.List;

/**
 * Created by Margo on 13.05.2015.
 */
@Named
@ViewScoped
public class MainBean implements Serializable {

    @Inject
    private ArticleServiceImpl articleService;

    private List<Article> articleList;

    @PostConstruct
    public void init() {
        articleList = articleService.getAllDescending();
    }

    public ArticleServiceImpl getArticleService() {
        return articleService;
    }

    public void setArticleService(ArticleServiceImpl articleService) {
        this.articleService = articleService;
    }

    public List<Article> getArticleList() {
        return articleList;
    }

    public void setArticleList(List<Article> articleList) {
        this.articleList = articleList;
    }
}
