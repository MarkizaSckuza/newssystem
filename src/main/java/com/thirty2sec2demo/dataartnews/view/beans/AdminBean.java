package com.thirty2sec2demo.dataartnews.view.beans;

import com.thirty2sec2demo.dataartnews.model.Article;
import com.thirty2sec2demo.dataartnews.model.User;
import com.thirty2sec2demo.dataartnews.service.impl.ArticleServiceImpl;
import com.thirty2sec2demo.dataartnews.service.impl.UserServiceImpl;

import javax.annotation.PostConstruct;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by Margo on 12.05.2015.
 */
@Named
@ViewScoped
public class AdminBean implements Serializable {

    @Inject
    private ArticleServiceImpl articleService;

    @Inject
    private UserServiceImpl userService;

    @Inject
    private Article article;

    private String title;
    private String content;
    private Date createdDate;
    private Date lastUpdatedDate;

    private User creator;

    @PostConstruct
    public void init() {
        creator = userService.find(1);
    }

    public void saveArticle() {
        setCreatedDate(new Date());
        setLastUpdatedDate(createdDate);
        article.setCreatedDate(createdDate);
        article.setLastUpdatedDate(lastUpdatedDate);
        article.setCreator(creator);
        articleService.create(article);
        redirectToArticle();
    }

    public void redirectToArticle() {
        ExternalContext context = FacesContext.getCurrentInstance().getExternalContext();
        try {
            context.redirect(context.getRequestContextPath() + "/admin/ViewArticle.xhtml?id=" + getArticle().getId());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //GETTERS & SETTERS
    public ArticleServiceImpl getArticleService() {
        return articleService;
    }

    public void setArticleService(ArticleServiceImpl articleService) {
        this.articleService = articleService;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public Date getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Date lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }
}
