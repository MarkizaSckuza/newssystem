package com.thirty2sec2demo.dataartnews.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.List;


@Entity
@Table(name = "tag")
public class Tag extends BaseObject{

    @Column(name = "tag")
    @NotNull
    private String tag;

    @ManyToMany(mappedBy = "tags")
    private List<Article> articles;

    //GETTERS & SETTERS

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }
}
