package com.thirty2sec2demo.dataartnews.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "role")
public class Role extends BaseObject {

    @Column
    @NotNull
    private String role;

    //GETTERS & SETTERS

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Role{");
        sb.append("id='").append(getId()).append('\'');
        sb.append("role='").append(role).append('\'');
        sb.append('}');
        return sb.toString();
    }
}
