package com.thirty2sec2demo.dataartnews.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * Created by Margo on 11.05.2015.
 */
@Entity()
@Table(name = "article_status")
public class ArticleStatus extends BaseObject{

    @Column(name = "status")
    @NotNull
    private String status;

}
