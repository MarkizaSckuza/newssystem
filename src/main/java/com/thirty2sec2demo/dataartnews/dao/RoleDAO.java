package com.thirty2sec2demo.dataartnews.dao;

import com.thirty2sec2demo.dataartnews.model.Role;

/**
 * Created by Margo on 11.05.2015.
 */
public interface RoleDAO extends BaseObjectDAO<Role>{
}
