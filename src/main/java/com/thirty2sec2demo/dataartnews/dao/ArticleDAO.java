package com.thirty2sec2demo.dataartnews.dao;

import com.thirty2sec2demo.dataartnews.model.Article;

import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
public interface ArticleDAO extends BaseObjectDAO<Article> {

    List<Article> getAllDescending();
}
