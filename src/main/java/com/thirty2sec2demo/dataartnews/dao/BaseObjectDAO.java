package com.thirty2sec2demo.dataartnews.dao;

import com.thirty2sec2demo.dataartnews.model.BaseObject;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
public interface BaseObjectDAO<T extends BaseObject> extends Serializable{

    void create(T object);

    void update(T object);

    T find(long id);

    List<T> getAll();

    void delete(T object);
}
