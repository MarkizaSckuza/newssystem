package com.thirty2sec2demo.dataartnews.dao;

import com.thirty2sec2demo.dataartnews.model.ArticleStatus;

/**
 * Created by Margo on 11.05.2015.
 */
public interface ArticleStatusDAO extends BaseObjectDAO<ArticleStatus> {
}
