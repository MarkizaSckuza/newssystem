package com.thirty2sec2demo.dataartnews.dao.impl;

import com.thirty2sec2demo.dataartnews.dao.ArticleStatusDAO;
import com.thirty2sec2demo.dataartnews.model.ArticleStatus;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Created by Margo on 11.05.2015.
 */
@Stateless
@LocalBean
public class ArticleStatusDAOImpl extends BaseObjectDAOImpl<ArticleStatus> implements ArticleStatusDAO {

    public ArticleStatusDAOImpl() { super(ArticleStatus.class);}
}
