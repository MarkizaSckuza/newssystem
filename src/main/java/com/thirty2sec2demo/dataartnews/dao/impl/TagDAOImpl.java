package com.thirty2sec2demo.dataartnews.dao.impl;

import com.thirty2sec2demo.dataartnews.dao.TagDAO;
import com.thirty2sec2demo.dataartnews.model.Tag;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Created by Margo on 11.05.2015.
 */
@Stateless
@LocalBean
public class TagDAOImpl extends BaseObjectDAOImpl<Tag> implements TagDAO {
    public TagDAOImpl() {
        super(Tag.class);
    }
}
