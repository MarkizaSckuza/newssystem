package com.thirty2sec2demo.dataartnews.dao.impl;

import com.thirty2sec2demo.dataartnews.model.Role;
import com.thirty2sec2demo.dataartnews.dao.RoleDAO;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Created by Margo on 10.05.2015.
 */
@Stateless
@LocalBean
public class RoleDAOImpl extends BaseObjectDAOImpl<Role> implements RoleDAO{

    public RoleDAOImpl() {
        super(Role.class);
    }
}
