package com.thirty2sec2demo.dataartnews.dao.impl;

import com.thirty2sec2demo.dataartnews.dao.BaseObjectDAO;
import com.thirty2sec2demo.dataartnews.model.BaseObject;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Margo on 10.05.2015.
 */

public class BaseObjectDAOImpl<T extends BaseObject> implements BaseObjectDAO<T> {

    private Class<T> type;

    @PersistenceContext
    protected EntityManager entityManager;

    public BaseObjectDAOImpl(Class<T> type) {
        this.type = type;
    }

    public void create(T object) {
        entityManager.persist(object);
    }

    @Override
    public void update(T object) {
        entityManager.merge(object);
    }

    @Override
    public T find(long id) {
        T result = entityManager.find(type, id);
        return result;
    }

    @Override
    public List<T> getAll() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<T> criteria = criteriaBuilder.createQuery(type);
        Root<T> root = criteria.from(type);
        criteria.orderBy(criteriaBuilder.asc(root.get("id")));
        List<T> list = entityManager.createQuery(criteria).getResultList();
        return list;
    }

    @Override
    public void delete(T object) {
        entityManager.remove(object);
    }
}
