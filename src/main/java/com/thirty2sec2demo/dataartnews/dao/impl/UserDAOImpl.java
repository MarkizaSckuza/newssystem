package com.thirty2sec2demo.dataartnews.dao.impl;

import com.thirty2sec2demo.dataartnews.dao.UserDAO;
import com.thirty2sec2demo.dataartnews.model.User;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Created by Margo on 11.05.2015.
 */
@Stateless
@LocalBean
public class UserDAOImpl extends BaseObjectDAOImpl<User> implements UserDAO {

    public UserDAOImpl() { super(User.class); }
}
