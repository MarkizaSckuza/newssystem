package com.thirty2sec2demo.dataartnews.dao.impl;

import com.thirty2sec2demo.dataartnews.dao.ArticleDAO;
import com.thirty2sec2demo.dataartnews.model.Article;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by Margo on 11.05.2015.
 */
@Stateless
@LocalBean
public class ArticleDAOImpl extends BaseObjectDAOImpl<Article> implements ArticleDAO {

    public ArticleDAOImpl() {
        super(Article.class);
    }

    @Override
    public List<Article> getAllDescending() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Article> criteria = criteriaBuilder.createQuery(Article.class);
        Root<Article> root = criteria.from(Article.class);
        criteria.orderBy(criteriaBuilder.desc(root.get("id")));
        List<Article> list = entityManager.createQuery(criteria).getResultList();
        return list;
    }
}
