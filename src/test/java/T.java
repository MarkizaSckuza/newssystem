import com.thirty2sec2demo.dataartnews.model.Role;
import junit.framework.TestCase;
import org.junit.Test;

import javax.persistence.*;

/**
 * Created by Margo on 10.05.2015.
 */

public class T extends TestCase{

    protected static EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("primary");
    protected EntityManager entityManager;
    protected EntityTransaction entityTransaction;


    @Test
    public void testSave() {
        entityManager = entityManagerFactory.createEntityManager();
        entityTransaction = entityManager.getTransaction();
        entityTransaction.begin();
        Role role = new Role();
        role.setRole("Margo");
        entityManager.persist(role);
        entityTransaction.commit();

    }
}
